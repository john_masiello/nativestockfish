# Do not specify minimum platform; the toolchain will use minimum sdk in gradle + ABI to infer min-
# imum platform support
# APP_PLATFORM := 11


MY_ABI := arm64-v8a armeabi-v7a

APP_ABI := $(MY_ABI)

# Use for mutex
APP_STL := gnustl_static
APP_OPTIM := release
NDK_TOOLCHAIN_VERSION := 4.9
