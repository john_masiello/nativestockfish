//
// Created by John on 5/21/2017.
//

#ifndef NATIVESTOCKFISH_START_STOCKFISH_H
#define NATIVESTOCKFISH_START_STOCKFISH_H


#include <iostream>

#include "bitboard.h"
#include "position.h"
#include "search.h"
#include "thread.h"
#include "tt.h"
#include "uci.h"
#include "syzygy/tbprobe.h"
#include "engine_io.h"

void startStockfish();

void exitStockfish();

#endif //NATIVESTOCKFISH_START_STOCKFISH_H
