//
// Created by John on 5/21/2017.
//

#include "start_stockfish.h"

namespace PSQT {
    void init();
}

void purgeMemory();

void startStockfish() {

    std::cout << engine_info() << std::endl;

    UCI::init(Options);
    PSQT::init();
    Bitboards::init();
    Position::init();
    Bitbases::init();
    Search::init();
    Pawns::init();
    Threads.init();
    Tablebases::init(Options["SyzygyPath"]);
    TT.resize(Options["Hash"]);
    UCI::init();
}

void exitStockfish() {

    // Join Threads, as well as detach their java counterparts
    Threads.main()->wait_for_search_finished();
    Threads.exit();

    // Remove global references
    Engine_IO::exit();

    // Clean up memory, if possible, since VM does not unload
    purgeMemory();
}

/// Release the memory burden on the VM
/// Deallocate anything expected to be deallocated, ie std::unique_ptr
void purgeMemory() {

    UCI::exit(Options);
    UCI::exit();

    // release anything consuming significant memory
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_john_nativestockfish_MainActivity_00024StockFishEngineThread_startStockfish(
        JNIEnv *env,
        jobject obj) {

    using namespace Engine_IO;

    env->GetJavaVM(&g_VM);

    // Store the environment for main thread, in order to both create and destroy global references
    main_env = env;
    set_g_stockFishEngine_thread(obj);

    set_mids();
    jni_string_response.init(main_env, get_mid_feedback());

    // Initialize the engine
    startStockfish();

    signalStockfishIsReady();
    jni_string_response.sendFeedbackToJava(engine_info());
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_john_nativestockfish_MainActivity_00024StockFishEngineThread_updateEngine(
        JNIEnv *env,
        jobject obj,
        jstring str) {

    const char* inputChars = env->GetStringUTFChars(str, nullptr);

    // Should block main thread
    Engine_IO::feedEngineInput(inputChars);

    env->ReleaseStringUTFChars(str, inputChars);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_john_nativestockfish_MainActivity_00024StockFishEngineThread_quitStockfish(
        JNIEnv *env,
        jobject obj) {

    exitStockfish();
}