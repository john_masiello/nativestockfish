//
// Created by John on 5/23/2017.
//

#include "engine_io.h"

const char* JAVA_CALLBACK_RECEIVE_FEEDBACK_NAME = "receiveEngineOutput";
const char* JAVA_CALLBACK_RECEIVE_FEEDBACK_SIG = "(Ljava/lang/String;)V";
const char* JAVA_CALLBACK_SIGNAL_READY_FOR_INPUT_NAME = "receiveEngineIsReady";
const char* JAVA_CALLBACK_SIGNAL_READY_FOR_INPUT_SIG = "()V";

using namespace std;


JNI_String_Builder::JNI_String_Builder() {
    sstream.str(std::string());
    sstream.clear();
}

void JNI_String_Builder::init(JNIEnv* env, jmethodID* mid) {
    JNI_String_Builder::env = env;
    JNI_String_Builder::mid = mid;
}

/// converts string to c-string to jstring
jstring JNI_String_Builder::make(std::string* string) {

    return env->NewStringUTF(string->c_str());
}

/// Convenience method for sendFeedbackToJava(sstream.str())
/// Post condition: clears and sets empty string to sstream
void JNI_String_Builder::sendFeedbackToJava() {

    sendFeedbackToJava(sstream.str());
    sstream.str(std::string());
    sstream.clear();
}

void JNI_String_Builder::sendFeedbackToJava(std::string string) {

 env->CallVoidMethod(Engine_IO::get_g_obj_stockFishEngine_thread(),
                             *mid,
                     make(&string));
}

namespace Engine_IO {

    JNI_String_Builder jni_string_response; /// Global variable; only intended for main thread

    // Android only allows 1 vm, so this vm serves as the vm for the engine
    JavaVM* g_VM;

    // jni interface pointer on the main thread;
    // used to remove the global reference
    JNIEnv* main_env;

    // Used to create clazz for JNI callbacks
    // This is global and should work across all of the native threads
    jobject g_obj_stockFishEngine_thread;
    jclass g_clz_stockFishEngine_thread;

    jmethodID jni_mid_feedback;
    jmethodID jni_mid_signal;

    /** precondition: main_env != nullptr
     * postcondition: g_obj_stockFishEngine_thread != nullptr
     * postcondition: g_clz_stockFishEngine_thread != nullptr
     */
    void set_g_stockFishEngine_thread(jobject g_stockFishEngine_thread) {

        if (main_env != nullptr) {
            g_obj_stockFishEngine_thread =
                    main_env->NewGlobalRef(g_stockFishEngine_thread);

            jclass myclass = main_env->GetObjectClass(g_obj_stockFishEngine_thread);
            g_clz_stockFishEngine_thread = (jclass)main_env->NewGlobalRef(myclass);
        }
    }

    /**
     *
     * @return the global object reference to stockfish engine thread
     */
    jobject get_g_obj_stockFishEngine_thread() {
        return g_obj_stockFishEngine_thread;
    }

    /**
     *
     * @return the global class reference to stockfish engine thread
     */
    jclass get_g_clz_stockFishEngine_thread() {
        return g_clz_stockFishEngine_thread;
    }

    /// Precondition: set main_env
    void set_mids() {

        jni_mid_feedback = main_env->GetMethodID(get_g_clz_stockFishEngine_thread(),
                                                 JAVA_CALLBACK_RECEIVE_FEEDBACK_NAME,
                                                 JAVA_CALLBACK_RECEIVE_FEEDBACK_SIG);

        jni_mid_signal = main_env->GetMethodID(get_g_clz_stockFishEngine_thread(),
        JAVA_CALLBACK_SIGNAL_READY_FOR_INPUT_NAME,
        JAVA_CALLBACK_SIGNAL_READY_FOR_INPUT_SIG);
    }

    /// Precondition: set_mids
    jmethodID* get_mid_feedback() {
        return &jni_mid_feedback;
    }

    jmethodID get_mid_signal() {
        return jni_mid_signal;
    }

    /// Sufficient Precondition: Engine_IO::set_g_stockFishEngine_thread
    void signalStockfishIsReady() {

        main_env->CallVoidMethod(get_g_obj_stockFishEngine_thread(), get_mid_signal());
    }

    /// Remove references to global variables
    void exit() {
        if (main_env != nullptr) {
            main_env->DeleteGlobalRef(g_obj_stockFishEngine_thread);
            main_env->DeleteGlobalRef(g_clz_stockFishEngine_thread);
        }
    };
}

#include "uci.h"
/// engages the loop by unlocking it and blocking the main thread
/// Checks if either looping thread or i is null
void Engine_IO::feedEngineInput(const char* i) {

    if (i) {
        UCI::processCommand(std::string(i));
    }
}