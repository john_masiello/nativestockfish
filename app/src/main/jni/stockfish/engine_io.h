//
// Created by John on 5/23/2017.
//

#ifndef NATIVESTOCKFISH_ENGINE_IO_H
#define NATIVESTOCKFISH_ENGINE_IO_H

#include <string>
#include <sstream>
#include "jni.h"

/**
 * use to build jstrings for jni interface
 */
class JNI_String_Builder {
public:
    JNIEnv* env;
    jmethodID* mid;
    std::stringstream sstream;

    JNI_String_Builder();
    void init(JNIEnv* env, jmethodID* mid);
    virtual jstring make(std::string* string);
    virtual void sendFeedbackToJava();
    void sendFeedbackToJava(std::string string);
};

extern const char* JAVA_CALLBACK_RECEIVE_FEEDBACK_NAME;
extern const char* JAVA_CALLBACK_RECEIVE_FEEDBACK_SIG;
extern const char* JAVA_CALLBACK_SIGNAL_READY_FOR_INPUT_NAME;
extern const char* JAVA_CALLBACK_SIGNAL_READY_FOR_INPUT_SIG;

namespace Engine_IO {

    extern JavaVM* g_VM;
    extern JNIEnv* main_env;
    extern JNI_String_Builder jni_string_response;

    void exit();

    void set_mids();
    jmethodID* get_mid_feedback();
    jmethodID get_mid_signal();

    void set_g_stockFishEngine_thread(jobject g_stockFishEngine_thread);
    jobject get_g_obj_stockFishEngine_thread();
    jclass get_g_clz_stockFishEngine_thread();

    void feedEngineInput(const char* i);
    void signalStockfishIsReady();
}


#endif //NATIVESTOCKFISH_ENGINE_IO_H
