LOCAL_PATH := $(call my-dir)

 SF_SRC_FILES := \
	 benchmark.cpp \
	 bitbase.cpp \
	 bitboard.cpp \
	 endgame.cpp \
	 evaluate.cpp \
	 engine_io.cpp \
	 start_stockfish.cpp \
	 material.cpp \
	 misc.cpp \
	 movegen.cpp \
	 movepick.cpp \
	 pawns.cpp \
	 position.cpp \
	 psqt.cpp \
	 search.cpp \
	 syzygy/tbprobe.cpp \
	 thread.cpp \
	 timeman.cpp \
	 tt.cpp \
	 uci.cpp \
	 ucioption.cpp

	 # Use preprocessor to tell stockfish whether it is running as 64-bit
	 # running 64-bit -> It will include definition IS_64BIT
	 MY_ARCH_DEF :=
     ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
       MY_ARCH_DEF += -DIS_64BIT
     endif
     ifeq ($(TARGET_ARCH_ABI),x86_64)
       MY_ARCH_DEF += -DIS_64BIT
     endif
     ifeq ($(TARGET_ARCH_ABI),mips64)
       MY_ARCH_DEF += -DIS_64BIT
     endif

include $(CLEAR_VARS)
LOCAL_MODULE    := stockfish
LOCAL_SRC_FILES := $(SF_SRC_FILES)
LOCAL_CFLAGS    := -std=c++11 -O0 $(MY_ARCH_DEF)

include $(BUILD_SHARED_LIBRARY)