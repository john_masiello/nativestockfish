package com.example.john.nativestockfish;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.LinkedList;

import timber.log.Timber;

import static android.view.inputmethod.EditorInfo.*;


interface UpdateUI {

    void appendToConsole(String newText);
}

public class MainActivity extends AppCompatActivity implements UpdateUI {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("stockfish");
    }

    private static class ChessBoardUIHandler extends Handler {
        private final WeakReference<MainActivity> mainActivity;

        private ChessBoardUIHandler(WeakReference<MainActivity> mainActivity) {
            super();
            this.mainActivity = mainActivity;
        }

        @Override
        public void handleMessage(Message msg) {
            mainActivity.get().consoleOutput.append("\n"+msg.getData().getString("stockfish_console"));
        }
    }

    private TextView consoleOutput;
    private Handler chessBoardUIHandler;
    private String position = BASE_POS;
    private String previousPosition = position;
    private String goWithTokens = BASE_GO;
    private static final String BASE_POS = "position startpos " + "" + " moves";
    private static final String BASE_GO = "go";

    private final Object stockfishThreadLock = new Object();
    private final StockFishEngineThread stockfishThread = new StockFishEngineThread(this,
            stockfishThreadLock);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {

            position = savedInstanceState.getString("POSITION_KEY");
            previousPosition = position;

            goWithTokens = savedInstanceState.getString("GO_WT_KEY");
        }

        consoleOutput = (TextView) findViewById(R.id.my_console_output);
        chessBoardUIHandler = new ChessBoardUIHandler(new WeakReference<>(this));

        EditText commandLine = (EditText) findViewById(R.id.cin);
        commandLine.setSelection(commandLine.getText().length());

        commandLine.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                switch (i) {
                    case IME_ACTION_DONE:


                        try {
                            handleText(textView.getText().toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        textView.clearFocus();

                        // consume text
                        textView.setText("");
                        return false;
                }

                return false;
            }
        });

        stockfishThread.start();

        synchronized (stockfishThreadLock) {
            stockfishThread.needToStartEngineFlag = true;
            stockfishThreadLock.notify();
        }
    }

    /**
     *
     * @param text
     * @return text.charAt(0)==' ' =>appends text to position. text equalsIgnorecase "clearpos" =>
     * position is reset to "position startpos", otherwise returns text
     * @throws Exception thrown if filter should consume the string and not call Stockfish
     */
    private String filterText(String text) throws Exception {
        if (text.equalsIgnoreCase("clearpos")) {
            position = BASE_POS;

            throw new Exception("consume clear position");
        }
        else if(text.charAt(0)==' ') {
            return position += text;
        }
        else {
            return text;
        }
    }

    private void handleText(String text) {

        switch (text.toLowerCase().trim()) {
            case "clearpos":
                position = BASE_POS;
                previousPosition = position;

                consoleOutput.append("\nclearpos");
                consoleOutput.append("\n"+position);
                break;

            case "echopos":
                consoleOutput.append("\nechopos");
                consoleOutput.append("\n"+position);
                break;

            case "prevpos":
                position = previousPosition;

                consoleOutput.append("\nprevpos");
                consoleOutput.append("\n"+position);
                break;

            default:

                if (text.charAt(0)==' ') {
                    previousPosition = position;
                    stockfishThread.enqueue(position += text);
                    stockfishThread.enqueue(goWithTokens);
                }
                else if (text.toLowerCase().startsWith("go bind")) {
                    goWithTokens = BASE_GO + " " + text.substring("go bind".length());
                    consoleOutput.append("\ngo with tokens: "+goWithTokens);
                }
                else {
                    stockfishThread.enqueue(text);
                }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        synchronized (stockfishThreadLock) {
            stockfishThread.quitFlag = true;
            stockfishThreadLock.notify();
        }
        joinThread(stockfishThread);

        chessBoardUIHandler.removeCallbacksAndMessages(null);
    }

    private void joinThread(Thread thread) {
        boolean joined = false;

        do {
            try {
                thread.join();
                joined = true;

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } while (!joined);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("POSITION_KEY", position);
        outState.putString("GO_WT_KEY", goWithTokens);
    }

    @Override
    public void appendToConsole(String newText) {

        Bundle data = new Bundle();
        data.putString("stockfish_console", newText);

        Message message = new Message();
        message.setData(data);

        chessBoardUIHandler.sendMessage(message);
    }

    //    /**
//     * A native method that is implemented by the 'native-lib' native library,
//     * which is packaged with this application.
//     */
//    public native String stringFromJNI();


    static class StockFishEngineThread extends Thread {

        /**
         * A native method that is implemented by the 'stockfish' native library,
         * which is packaged with this application.
         */
        private native void startStockfish();

        /**
         * <p>A native method that is implemented by the 'stockfish' native library,
         * which is packaged with this application.</p>
         */
        public native void updateEngine(String o);

        /**
         * <p>A native method that is implemented by the 'stockfish' native library,
         * which is packaged with this application.</p>
         */
        private native void quitStockfish();

        /**
         * A java callback to the native
         * @param o
         */
        public void receiveEngineOutput(String o) {

            activityUI.appendToConsole(o);
        }

        /**
         * A java callback to the native
         */
        public void receiveEngineIsReady() {

            synchronized (stockfishThreadLock) {

                engineIsReadyForInput = true;
                stockfishThreadLock.notify();
            }
        }

        private final @NonNull UpdateUI activityUI;
        private final @NonNull Object stockfishThreadLock;

        boolean needToStartEngineFlag = false;
        boolean engineIsReadyForInput = false;
        boolean quitFlag = false;

        private LinkedList<String> queue;

        StockFishEngineThread(@NonNull UpdateUI activityUI, @NonNull Object stockfishThreadLock) {

            super();
            this.activityUI = activityUI;
            this.stockfishThreadLock = stockfishThreadLock;
            queue = new LinkedList<>();
        }

        public void enqueue(String input) {

            synchronized (stockfishThreadLock) {
                queue.addLast(input);
                stockfishThreadLock.notify();
            }
        }

        @Override
        public void run() {

            boolean queueIsEmpty;

            while (!quitFlag) {

                if (needToStartEngineFlag) {

                    needToStartEngineFlag = false;

                    startStockfish();
                }

                synchronized (stockfishThreadLock) {
                    queueIsEmpty = queue.isEmpty();

                    if (!queueIsEmpty && engineIsReadyForInput && !quitFlag) {

                        engineIsReadyForInput = false;

                        updateEngine(queue.pop());
                    }
                }

                synchronized (stockfishThreadLock) {
                    try {
                        if ((queueIsEmpty || !engineIsReadyForInput) && !quitFlag) {
                            stockfishThreadLock.wait();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            quitStockfish();
        }
    }
}
